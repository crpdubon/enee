<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <title>Sucursales Enee</title>
    <link rel="stylesheet" href="Css.css">
</head>

<body>
    <font color="#87CEEB" face="Rockwell">
        <h1 align="center"><i>Formulario De Sucursales de la Enee </i></h1>
    </font>

    <center>
        <img src="Enee.jpeg">
    </center>

    <div id="Sucursales" style="position:absolute;left:180px;top:300px;width:900px;height:650px;z-index:22;">

        <form name="Sucursales" action="InsertarSucursales.php" method="POST">
            <div>
                <label for="Lbl_Nomb_Suc" id="Lbl_Nomb_Suc"
                    style="position:absolute;left:40px;top:40px;width:300px;height:22px;line-height:22px;z-index:0;">Nombre
                    de Sucursal: </label>
                <input name="Tbx_Nomb_Suc" id="Tbx_Nomb_Suc" type="text"
                    style="position:absolute;left:300px;top:40px;width:550px;height:20px;z-index:10;"><br>
            </div>
            <div>
                <label for="Lbl_Nomb_Adm"
                    style="position:absolute;left:40px;top:90px;width:300px;height:22px;line-height:22px;z-index:1;">Nombre
                    del Administrador: </label>
                <input name="Tbx_Nomb_Admin" id="Tbx_Nomb_Admin" type="text"
                    style="position:absolute;left:300px;top:90px;width:550px;height:20px;z-index:11;"><br>
            </div>
            <div>
                <label for="Lbl_Telefono"
                    style="position:absolute;left:40px;top:140px;width:300px;height:22px;line-height:22px;z-index:2;">Telefono:
                </label>
                <input name="Tbx_Telefono" id="Tbx_Telefono" type="tel"
                    style="position:absolute;left:300px;top:140px;width:550px;height:20px;z-index:12;"><br>
            </div>
            <div>
                <label for="Lbl_Dir"
                    style="position:absolute;left:40px;top:190px;width:300px;height:22px;line-height:22px;z-index:3;">Dirección:</label>
                <textarea name="Tbx_Dir" id="Tbx_Dir" cols="20" rows="5"
                    style="position:absolute;left:300px;top:190px;width:550px;height:130px;z-index:13;"></textarea><br>
            </div>
            <div>
                <label for="Lbl_Fax"
                    style="position:absolute;left:40px;top:350px;width:300px;height:22px;line-height:22px;z-index:4;">Fax:
                </label>
                <input name="Tbx_Fax" id="Tbx_Fax" type="tel"
                    style="position:absolute;left:300px;top:350px;width:550px;height:20px;z-index:14;"><br>
            </div>
            <div>
                <label for="Lbl_Pedidos"
                    style="position:absolute;left:40px;top:400px;width:300px;height:22px;line-height:22px;z-index:5;">Pedidos:
                </label>
                <input name="Tbx_Pedidos" id="Tbx_Pedidos" type="number"
                    style="position:absolute;left:300px;top:400px;width:550px;height:20px;z-index:15;"><br>
            </div>
            <div>
                <label for="Lbl_F_Creacion"
                    style="position:absolute;left:40px;top:450px;width:300px;height:22px;line-height:22px;z-index:6;">Fecha
                    de Creación: </label>
                <input name="Tbx_F_Creacion" id="Tbx_F_Creacion" type="date"
                    style="position:absolute;left:300px;top:450px;width:550px;height:20px;z-index:16;"><br>
            </div>
            <div>
                <label for="Lbl_F_Mod"
                    style="position:absolute;left:40px;top:500px;width:300px;height:22px;line-height:22px;z-index:7;">Fecha
                    de Modificación: </label>
                <input name="Tbx_F_Mod" id="Tbx_F_Mod" type="date"
                    style="position:absolute;left:300px;top:500px;width:550px;height:20px;z-index:17;"><br>
            </div>
            <div>
                <input name="Btn_Insertar" id="Btn_Insertar" value="Insertar" type="submit"
                    style="position:absolute;left:300px;top:600px;width:89px;height:23px;z-index:21;">
                <input name="Btn_Limpiar" id="Btn_Limpiar" value="Limpiar" type="reset"
                    style="position:absolute;left:500px;top:600px;width:89px;height:23px;z-index:22;"><br>
            </div>
        </form>
    </div>

    <div id="Tabla_Sucursales" style="position:absolute;left:10px;top:1000px;width:2000px;height:570px;z-index:22;">
        <table>
            <tr>
                <td align="center" width="300" style="border:2pt solid #804000">Nombre de Sucursal</td>
                <td align="center" width="300" style="border:2pt solid #804000">Nombre del Administrador</td>
                <td align="center" width="200" style="border:2pt solid #804000">Teléfono</td>
                <td align="center" width="300" style="border:2pt solid #804000">Dirección</td>
                <td align="center" width="200" style="border:2pt solid #804000">Fax</td>
                <td align="center" width="100" style="border:2pt solid #804000">Pedidos</td>
                <td align="center" width="200" style="border:2pt solid #804000">Fecha de Creación</td>
                <td align="center" width="200" style="border:2pt solid #804000"> <b>Fecha de Modificación</b></td>
                <td align="center" width="100" style="border:2pt solid #804000"></td>
                <td align="center" width="100" style="border:2pt solid #804000"></td>
            </tr>
            <tbody>
                <?php include 'SeleccionarSucursales.php'; ?>
            </tbody>
        </table>
    </div>

</body>
<footer>
    <div id="Pie" style="position:absolute;left:10px;top:1600px;width:2000px;height:570px;z-index:22;">

        <p id='visor_imagenes'><span class="blanco-rojo">
          Autor: Carlos R. Pérez Dubón<br>
          Correo: <a href="mailto:crpdubon@gmail.com">crpdubon@gmail.com</a><br>
          Youtube: <a href="https://bit.ly/3HpDQ3y">Canal</a><br>
          twitter: <a href="https://twitter.com/turinds2">Perfil</a><br>
          instagram: <a href="https://www.instagram.com/turinds2/">Perfil</a><br>
          </span></p>
    </div>
</footer>

</html>