-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: remotemysql.com
-- Tiempo de generación: 23-09-2022 a las 20:21:41
-- Versión del servidor: 8.0.13-4
-- Versión de PHP: 7.3.33-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `6DixxQP6HR`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `Sucursales`
--

CREATE TABLE `Sucursales` (
  `Nomb_Suc` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Nomb_Admin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `Telefono` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `Dir` text COLLATE utf8_unicode_ci NOT NULL,
  `Fax` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Pedidos` int(11) DEFAULT NULL,
  `F_Creacion` date DEFAULT NULL,
  `F_Mod` date DEFAULT NULL,
  `Activo` tinyint(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `Sucursales`
--

INSERT INTO `Sucursales` (`Nomb_Suc`, `Nomb_Admin`, `Telefono`, `Dir`, `Fax`, `Pedidos`, `F_Creacion`, `F_Mod`, `Activo`) VALUES
('Enee Colomonacagua', 'Denia López', '+504-9962-7108', 'Llano Grande', '+504-9962-7108', 6, '2022-09-23', '2022-09-23', 1),
('Enee Comayaguela', 'Noel Díaz', '+504-9830-0053', 'Cerro Grande Zona 4', '+504-9830-0053', 4, '2022-09-23', '2022-09-23', 1),
('Enee Intibucá', 'Marily López', '+504-8767-0560', 'Barrio Buenos Aires', '+504-8767-0560', 2, '2022-09-23', '2022-09-23', 1),
('Enee Nacaome', 'Lester Guerra', '+504-2795-3359', 'Barrio El Centro', '+504-2795-3359', 5, '2022-09-23', '2022-09-23', 1),
('Enee Tegucigalpa', 'Carlos Dubón', '+504-7214-0981', 'Barrio Buenos Aires', '+504-7214-0981', 3, '2022-09-23', '2022-09-23', 1),
('Santa Rosa de Copán', 'Ligia Alexandra Pérez', '+504-3240-7495', 'Villa Belén', '+504-3240-7495', 1, '2022-09-23', '2022-09-23', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `Sucursales`
--
ALTER TABLE `Sucursales`
  ADD PRIMARY KEY (`Nomb_Suc`),
  ADD KEY `Nomb_Admin` (`Nomb_Admin`),
  ADD KEY `Telefono` (`Telefono`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
