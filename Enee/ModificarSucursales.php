<?php
include 'Conexion.php';

if ($Conexion) {
    //echo "<h2>***Conexión Exitosa***</h2><br>";

    $sql = "select * from Sucursales where Nomb_Suc='" . $_GET['Nomb_Suc'] . "' ;";
    $Resultado = $Conexion->query($sql);
    $Fila = $Resultado->fetch_assoc();
    $Consulta = [
        $Fila['Nomb_Suc'], $Fila['Nomb_Admin'], $Fila['Telefono'],
        $Fila['Dir'], $Fila['Fax'], $Fila['Pedidos'], $Fila['F_Creacion'],
        $Fila['F_Mod'],
    ];
}
?>

<!DOCTYPE html>
<html>
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Modificar Sucursal</title>
<link rel="stylesheet" href="Css.css">
</head>
<body>
<div class="todo">
  <div id="contenido">
  	<div style="margin: auto; width: 800px; border-collapse: separate; border-spacing: 10px 5px;">
  		<span> <h1>Modificar Producto</h1> </span>
  		<br>
	  <form action="ActualizarSucursales.php" method="POST" style="border-collapse: separate; border-spacing: 10px 5px;">
      <div>
        <input name="Tbx_Nomb_Suc" id="Tbx_Nomb_Suc" type="hidden" value="<?php echo $_GET['Nomb_Suc'] ?>"><br>
      </div>
      <div>
        <label for="Lbl_Nomb_Adm">Nombre del Administrador: </label>
        <input name="Tbx_Nomb_Admin" id="Tbx_Nomb_Admin" type="text" value="<?php echo $Consulta[1] ?>"><br>
      </div>
      <div>
        <label for="Lbl_Telefono">Telefono: </label>
        <input name="Tbx_Telefono" id="Tbx_Telefono" type="tel" value="<?php echo $Consulta[2] ?>"><br>
      </div>
      <div>
        <label for="Lbl_Dir">Dirección:</label>
        <textarea name="Tbx_Dir" id="Tbx_Dir" cols="20" rows="5" ><?php echo $Consulta[3] ?></textarea><br>
      </div>
      <div>
        <label for="Lbl_Fax">Fax: </label>
        <input name="Tbx_Fax" id="Tbx_Fax" type="tel" value="<?php echo $Consulta[4] ?>"><br>
      </div>
      <div>
        <label for="Lbl_Pedidos">Pedidos: </label>
        <input name="Tbx_Pedidos" id="Tbx_Pedidos" type="number" value="<?php echo $Consulta[5] ?>"><br>
      </div>
      <div>
        <label for="Lbl_F_Creacion">Fecha de Creación: </label>
        <input name="Tbx_F_Creacion" id="Tbx_F_Creacion" type="date" value="<?php echo $Consulta[6] ?>"><br>
      </div>
      <div>
        <label for="Lbl_F_Mod">Fecha de Modificación: </label>
        <input name="Tbx_F_Mod" id="Tbx_F_Mod" type="date" value="<?php echo $Consulta[7] ?>"><br>
      </div>
      <div>
 		<button type="submit" class="btn btn-success">Guardar</button>
        </div>
     </form>
  	</div>

  </div>

</div>


</body>
</html>