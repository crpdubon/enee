<?php
include 'Conexion.php';

if ($Conexion) {
    //echo "<h2>***Conexión Exitosa***</h2><br>";

    $sql = "select * from Sucursales;";
    $Resultado = $Conexion->query($sql);

    while ($Fila = $Resultado->fetch_assoc()) {
        $Nomb_Suc = $Fila['Nomb_Suc'];
        $Nomb_Admin = $Fila['Nomb_Admin'];
        $Telefono = $Fila['Telefono'];
        $Dir = $Fila['Dir'];
        $Fax = $Fila['Fax'];
        $Pedidos = $Fila['Pedidos'];
        $F_Creacion = $Fila['F_Creacion'];
        $F_Mod = $Fila['F_Mod'];
        $Activo = $Fila['Activo'];
        if( $Activo==1 ){
            echo "<tr>";
            echo "<td align=\"center\" width=\"150\" style=\"border:2pt solid #804000\">" . $Nomb_Suc . "</td>";
            echo "<td align=\"center\" width=\"200\" style=\"border:2pt solid #804000\">" . $Nomb_Admin . "</td>";
            echo "<td align=\"center\" width=\"150\" style=\"border:2pt solid #804000\">" . $Telefono . "</td>";
            echo "<td align=\"center\" width=\"300\" style=\"border:2pt solid #804000\">" . $Dir . "</td>";
            echo "<td align=\"center\" width=\"150\" style=\"border:2pt solid #804000\">" . $Fax . "</td>";
            echo "<td align=\"center\" width=\"100\" style=\"border:2pt solid #804000\">" . $Pedidos . "</td>";
            echo "<td align=\"center\" width=\"100\" style=\"border:2pt solid #804000\">" . $F_Creacion . "</td>";
            echo "<td align=\"center\" width=\"100\" style=\"border:2pt solid #804000\">" . $F_Mod . "</td>";
            echo "<td align=\"center\" width=\"100\" style=\"border:2pt solid #804000\"><a href='ModificarSucursales.php?Nomb_Suc=" . $Fila['Nomb_Suc'] . "'><input type='submit' name='Btn_Modificar' value='Modificar'/></td> \n";
            echo "<td align=\"center\" width=\"100\" style=\"border:2pt solid #804000\"><a href='EliminarSucursales.php?Nomb_Suc=" . $Fila['Nomb_Suc'] . "'><input type='submit' name='Btn_Eliminar' value='Eliminar'/></td> \n";
            echo "</tr>";
        }
    }
    $Conexion->close();
} else {
    echo "<h2>NO fue Posible establecer Conexión!!!<br>Revise los Parámetros de la Misma!!!<br></h2>";
}
