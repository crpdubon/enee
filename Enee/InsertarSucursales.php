<?php
include 'Conexion.php';

if ($Conexion) {
    if (!empty($_POST['Tbx_Nomb_Suc']) && !empty($_POST['Tbx_Nomb_Admin']) && !empty($_POST['Tbx_Telefono']) && !empty($_POST['Tbx_Dir'])) {
        $Nomb_Suc = $_POST["Tbx_Nomb_Suc"];
        $Nomb_Admin = $_POST["Tbx_Nomb_Admin"];
        $Telefono = $_POST["Tbx_Telefono"];
        $Dir = $_POST["Tbx_Dir"];
        $Fax = $_POST["Tbx_Fax"];
        $Pedidos = $_POST["Tbx_Pedidos"];
        if (!empty($_POST["Tbx_F_Creacion"])) {
            $F_Creacion = $_POST["Tbx_F_Creacion"];
        } else {
            $F_Creacion = date('Y-m-d', time());
        }
        if (!empty($_POST["Tbx_F_Mod"])) {
            $F_Mod = $_POST["Tbx_F_Mod"];
        } else {
            $F_Mod = date('Y-m-d', time());
        }

        $sql = "INSERT INTO Sucursales(Nomb_Suc, Nomb_Admin, Telefono, Dir, Fax, Pedidos, F_Creacion, F_Mod, Activo)
                  values ('$Nomb_Suc','$Nomb_Admin','$Telefono','$Dir','$Fax','$Pedidos','$F_Creacion','$F_Mod','1');";
        if ($Conexion->query($sql)) {
            $Conexion->close();
            echo '<script>alert("Datos Guardados Exitosamente!!!");</script>';
            echo '<script>window.location.replace("Sucursales.php");</script>';
        } else {
            echo '<script>alert("NO Fue Posible realizar la Inserción!!!");</script>';
            echo '<script>window. history. back();</script>';
        }
    } else {
        echo '<script>alert("NO Deben Haber Campos Vacíos!!!");</script>';
        echo '<script>window. history. back();</script>';
    }
}
